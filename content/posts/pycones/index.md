+++
title = "Charla en el PyConES"
date = 2022-08-18
[taxonomies]
tags = ["talks", "python"]
+++

Este año vuelvo a contribuir en el congreso nacional de Python (PythonConES).

{{rimage(path="pycones.svg", scale="50%")}}

Prepararé un taller de dos horas sobre identificación de cuellos de botella en
un programa en Python y mejora de eficiencia. Se me ocurrió dicho taller viendo
la experiencia de alumnos míos en Metaheurísticas, que escogieron el uso de
Python, y por no tener suficiente experiencia el tiempo de ejecución era
demasiado largo. Ya informé que no recomendaba usar Python si no se tenía algo
de experiencia sobre código eficiente en Python, pero algunos no siguieron mi
recomendación y se encontraron con problemas. Y luego pensando, decidí que
estaría bien un taller sobre lo que debería de saber para poder haberlo hecho
bien.

La charla será el próximo día 30 de Septiembre de 15:30-17:30 en la
[ETSIIT](https://etsiit.ugr.es/). Puedes consultarlo en el [Horario del
PyConES](https://charlas.2022.es.pycon.org/pycones2022/schedule/). También
supongo que será difundido por _streaming_, y tras la charla se colgará, pondré
el enlace en cuanto esté disponible para quien no pueda asistir pueda verlo luego.

Aprovecho para animaros a asistir, que tiene charlas muy interesantes, y los
organizadores son colegas :wink:.

A continuación describo más en detalle la charla:

Determinaremos el código que supone el mayor consumo de recursos (usando un
profile). También veremos cómo podemos optimizar las partes críticas usando
distintas técnicas. Se espera que tras el taller los asistentes sepan a
identificar los cuellos de botella de sus propios programas/librerías, y
conozcan distintas opciones para poder optimizarlos.

El taller consta de de un enfoque práctico, en el que se verán el uso de las
librerías y técnicas sobre algunos ejemplos prácticos. En particular se verá:

- Hacer un profile de un programa para detectar cuellos de botella.
- Hacer un profile desde un notebook.

Una vez identificado el cuello de botella, pasaremos optimizarlo. Para ello,
aparte de ciertos consejos de implementación, se mostrará cómo pueden reducirse
utilizando algunas librerías eficientes y cython para llamar a código en C/C++.
Dada la duración no se explicarán en detalle esas librerías, pero sí servirá
para darlas a conocer a quien no las conozcan y mostrar cómo adaptando un
pequeño porcentaje del código se pueden obtener muchos mejores resultados.

Las librerías que veremos son:
- Librerías cProfile de la librería estándar.
- pprofile, mejor rendimiento.
- scalene, una librería de profile moderna y con muchas características.
- Aplicación de scipy y/o numpy para optimizar código.
- Introducir numba y PyPy.
- cython para optimizar partes críticas.


[https://charlas.2022.es.pycon.org/pycones2022/talk/VTQDSZ/](https://charlas.2022.es.pycon.org/pycones2022/talk/VTQDSZ/)
