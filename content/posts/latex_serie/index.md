+++
title = "Escribiendo Documentación fácilmente en Latex"
author = ["Daniel Molina"]
date = 2023-10-29
tags = ["latex"]
draft = true
[taxonomies]
  tags = ["latex"]
+++

Escribir latex es fácil si se sabe, sin embargo, la curva de aprendizaje puede ser complicada.

A menudo, los estudiantes deciden, o les recomiendan, documentar unas prácticas, o el TFG en Latex, y se encuentran con dificultades. Pueden empezar con plantillas que no conocen, y cualquier cambio se les hace un mundo. También se encuentran con problemas por tener experiencia en Word, y querer que.

Hay mucha información sobre Latex, pero la mayoría está muy pensada para personas con necesidad de escribir textos científicos (a lo que está más enfocado), y no está dirigida a personas como los estudiantes que tienen otras prioridades.

Este es un primer documento de una serie, pensadas para una charla introductoria para darle con la **Oficina de Software Libre** de la Universidad de Granada (UGR).

La estructura es la siguiente:

<div class="ox-hugo-toc toc">

<div class="heading">&Iacute;ndice</div>

- [Instalación](#instalación)
- [Primer documento](#primer-documento)

</div>
<!--endtoc-->


## Instalación {#instalación}

Ha menudo esta opción se obvia, ya que es siempre es posible usar la estupenda página <https://www.overleaf.com/> con la que se puede permitir ejecutar.

{{rimage(path="overleaf.png", scale="80%")}}

Esa es la primera opción, y la más sencilla.

Tiene como ventajas:

-   No hace falta instalar nada.

-   Fácil de usar.

-   Trabajo colaborativo.

-   Se integra con Git, con lo que se puede trabajar con otros entornos y sincronizar.

Tiene como desventajas:

-   Requiere conexión.

-   Ciertas funcionalidades, como revisión de cambios, requiere pagar.

Yo suele trabajo usando overleaf a menudo, pero trabajando en local, y sincronizando con Git con el resto de autores.

Para trabajar en local, hay múltiples entornos:


## Primer documento {#primer-documento}
