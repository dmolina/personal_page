+++
title = "Introducción a Julia"
date = 2020-01-20
[taxonomies]
tags = ["julia"]
+++

El año pasado empecé a trabajar en un lenguaje de programación prometedor, 
[Julia](<http://julialang.org/>).

Incluso dí una charla orientada a personas con conocimientos de programación 
(lo comparo mucho en la charla con Python).

[Charla en Castellano](https://github.com/dmolina/julia%5Fpresentacion/blob/master/Julia%5FPresentacion%5Fslides.pdf)
![](julia_intro_es.png)

The resources of that talk are available at [Github
Repository](<https://github.com/dmolina/julia%5Fpresentacion/>).
