+++
title = "Introduction to Julia"
date = 2020-01-20
[taxonomies]
tags = ["julia"]
+++

The last year I have been working in a promising programming language,
[Julia](<http://julialang.org/>).

Even I have done a presentation focused on people with Computer Science
background (so the talk compares Julia against Python),

[Talk in English](https://github.com/dmolina/julia%5Fpresentacion/raw/master/Julia%5FPresentation%5Fslides.pdf)
![julia_intro](julia_intro_en.png)

The resources of that talk are available at [Github
Repository](<https://github.com/dmolina/julia%5Fpresentacion/>).
