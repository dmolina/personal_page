+++
title = "Talk in JuliaCon2022"
date = 2022-07-27
[taxonomies]
tags = ["teaching", "julia"]
+++

Today you can watch my talk in JuliaCon 2022, about my teaching experiences
using Julia.

It can be seen here:
[https://live.juliacon.org/talk/KGLNUH](https://live.juliacon.org/talk/KGLNUH)

After the talk, you will watch it in Youtube link.

The source code of the demos is available at:
[https://gitlab.com/daniel.molina/mh_demos/](https://gitlab.com/daniel.molina/mh_demos/)
