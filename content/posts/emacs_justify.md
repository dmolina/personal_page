+++
title = "Fill-more o la importancia de leer la documentación"
date = 2017-12-15
[taxonomies]
tags = ["emacs"]
+++

Me **encanta*** Emacs y el modo auto-fill. Cuando trabajo lo uso siempre para
hacer más fácil leer el texto (con un pequeño valor, con 80 o 100). Luego, si
tengo que copiarlo a un documento Word (cuando colaboro con otros/as) o en un
texto  (como someter una revisión) simplemene ajusto el `fill-column` a un valor
muy alto (2000 o similar), con C-x f, y luego copio todo el texto.

Hasta ahora había sufrido en silencio un pequeño problema en modo texto
_text-mode_ (no en org-mode). Si pones

```sh
Texto.

- Item 1.
- Item 2.
```

Después del `fill-mode`, tienes:

```sh
Text.

- Item 1 Item 2.
```

Y para arreglarlo tienes que poner una línea entre ambas:

```sh
Texto.

- Item 1.

- Item 2.
```

(La línea entre Teto y el primer item también es necesaria).

Pensé que era algo inevitable, pero leyendo la documentación,

<https://www.emacswiki.org/emacs/FillParagraph>

He aprendido que con una simple línea en elisp ese comportamiento se arregla:

```elisp
;; The original value is "\f\\|[      ]*$", so we add the bullets (-), (+), and (*).
;; There is no need for "^" as the regexp is matched at the beginning of line.
(setq paragraph-start "\f\\|[ \t]*$\\|[ \t]*[-+*] ")
```

Debo de comprobar la documentación disponible más a menudo :-).
