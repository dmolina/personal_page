+++
title = "Automatic list of papers using Zola and Bibtex"
date = 2023-08-17
[taxonomies]
tags = ["research"]
+++

I haven't posted on the Blog for a long time due to lack of time (and desire on vacation).

But I've just tweaked it to add the publications, and as it's something that I've searched a lot on the Internet and I haven't found anything, I'm publishing it here: How to automatic list my papers using Zola and Bibtex.

The first thing I did was to create a .bib file with all my publications. Luckily it was the easiest thing to do, I used [Scopus](https://www.scopus.com/) to do it.

Once copied to a /data/ directory I created a page with a new template, *paper_lists.html*.

The bulk of that page is the following instruction in the header:

```
template = "papers_list.html"
```

Next let's look at the `papers_list.html` file:

I first load the file into the _data_ variable:

```html
{% set data = load_data(path="/data/all.bib", format="bibtex") -%}
```

In `data.bibliographies` I have a vector of publications. Apart from the `entry_type` attribute that indicates the type of publication (I filter by "ARTICLE") there is the `tags` attribute that contains the typical attributes of a paper: title, author, journal, year, volume, pages, ....

First I filter by the articles sorted by year:

```html
{% set sorted_bib = data.bibliographies | filter(attribute="entry_type", value="ARTICLE") %}
{% for bib in sorted_bib | sort(attribute="tags.year") | reverse %}
...
{% endfor %}
```

Since I want to show it by year, I want to show only the year if it changes from the previous one. For this I use a global variable `last_year`:

```html
{% if last_year != bib.tags.year %}
	    <h1 class="title">{{bib.tags.year}}</h1>
	    {% set_global last_year = bib.tags.year %}
{% endif %}
```

The rest is simple, just use `bib.tags`. I have tried to use macros, but it has limitations. 

I show now the complete code:

```html
{% set data = load_data(path="/data/all.bib", format="bibtex") -%}

{% if data %}
<section class="section">
    <div class="container">
	<div class="columns is-centered">
	    {% set_global last_year="2000" %}
	<div class="column is-9">
	    <h2 class="title">{{trans(key="papers",lang=lang)}}</h2>
	    {% set sorted_bib = data.bibliographies | filter(attribute="entry_type", value="ARTICLE") %}
	    {% for bib in sorted_bib | sort(attribute="tags.year") | reverse %}
	    {% if last_year != bib.tags.year %}
	    <h1 class="title">{{bib.tags.year}}</h1>
	       {% set_global last_year = bib.tags.year %}
	    {% endif %}
	    {# __tera_context#}
	    <article class="box">
            <h2>
		<b>
		{% if bib.tags.doi %}
		<a class="has-text-dark" href='http://dx.doi.org/{{bib.tags.doi}}' target="_blank">
            {{ bib.tags.title }}
		</a>
		{% else %}
		{{ bib.tags.title }}
		{% endif %}
		</b>{{ bib.tags.author}}. {{ bib.tags.journal}}. vol: {{bib.tags.volume}}, {% if bib.tags.pages %}pages: {{bib.tags.pages}}{% else %} In Press {% endif %} ({{bib.tags.year}}).
		{#
		{% if bib.tags.note != "cited By 0" %}
		{{bib.tags.note}}
		{% else %}
		{{bib.tags.note}}
		{% endif %}
		#}
		{% if bib.tags.note and bib.tags.note != "Cited By 0" %}
		{% set citations = bib.tags.note | split(pat=" ") | last | int %}
		{% if citations >= 5  %}
		<b>{{trans(key="citations", lang=lang)}}: {{ citations }}</b>
		{% endif %}
		{% endif %}
	    </h2>
	</article>
	  {% endfor %}
      </div>
    </div>
</section>
{% endif %}
```
