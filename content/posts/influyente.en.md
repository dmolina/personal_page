+++
title = "Listed as one of the most influential researchers"
date = 2023-10-18
[taxonomies]
tags = ["research"]
+++

My name has appeared on the list of the most influential researchers in the world, according to the Stanford ranking, which considers those researchers in all fields who receive the most influence (citations).

News (In Spanish):

[https://www.granadahoy.com/granada/Universidad-Granada-investigadores-influyentes-Stanford_0_1838516842.html](https://www.granadahoy.com/granada/Universidad-Granada-investigadores-influyentes-Stanford_0_1838516842.html)

[https://www.ugr.es/universidad/noticias/ugr-record-154-investigadores-mas-influyentes-mundo](https://www.ugr.es/universidad/noticias/ugr-record-154-investigadores-mas-influyentes-mundo)

And he was in that element 114538, which may not seem much, but taking into account that the 2% of the most influential reach up to 209602 names, it is clear that it is an important merit.

Moreover, only 154 scientists from the UGR are in that list, from all areas, and I am positioned near the middle of the list according to Stanford's ordered list, in a better position than researchers with more experience and recognised professors, which is quite an achievement 👍.
