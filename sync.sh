rm config.toml
cp config.codeberg.toml config.toml
zola build
mv public to_pages

if ! [ -d pages ]; then
    git clone git@codeberg.org:dmolina/pages.git
else
    cd pages
    git pull
    cd ..
fi

rsync -a to_pages/* pages
cd pages
git add -A
git commit -m "New version"
git push origin HEAD
cd ..
rm -Rf to_pages
# Copy to local website
zola -c config_danimolina.toml build --force -o pages_dmolina
cp config_danimolina.toml pages_dmolina
rsync -a pages_dmolina/* daniel@tacolab.org:~/personal_page/
rm -Rf pages_dmolina
# Use the gitlab configuration as nortmal
rm config.toml
cp config.gitlab.toml config.toml
